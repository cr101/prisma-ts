import { FieldResolver } from 'nexus/dist/typegenTypeHelpers';
import { ObjectDefinitionBlock } from 'nexus/dist/definitions/objectType';

export type MutationResolver<ResolverName extends string> = FieldResolver<'Mutation', ResolverName>;
export type QueryResolver<ResolverName extends string> = FieldResolver<'Query', ResolverName>;

export type QueryDefinition = (t: ObjectDefinitionBlock<'Query'>) => void;
export type MutationDefinition = (t: ObjectDefinitionBlock<'Mutation'>) => void;
