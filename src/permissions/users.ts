import { rule } from 'graphql-shield';

import { getUserId } from '../utils/auth';

const isAuthenticated = rule()(async (parent, args, context) => {
  const userId = await getUserId(context);
  return Boolean(userId);
});

export { isAuthenticated };
