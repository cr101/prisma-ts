import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { applyMiddleware } from 'graphql-middleware';

import { permissions } from './permissions';
import { schema } from './schema';
import { createContext } from './context';
import session from './config/session';

const server = new ApolloServer({
  schema: applyMiddleware(schema, permissions),
  context: createContext,
});

const app = express();

if (process.env.NODE_ENV === 'production') {
  app.set('trust proxy', 1);
}

app.use(session);

server.applyMiddleware({
  app,
  cors: {
    credentials: true,
    origin: new RegExp(process.env.ORIGIN_DOMAIN || '.*'),
  },
});

app.listen(process.env.PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`🚀 Server ready at: http://localhost:${process.env.PORT}`);
});
