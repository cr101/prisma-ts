import { Photon } from '@prisma/photon';
import { Request } from 'express';

const photon = new Photon();

export interface Context {
  photon: Photon;
  request: Request;
}

export function createContext({ req }: { req: Request }): Context {
  return {
    request: req,
    photon,
  };
}
