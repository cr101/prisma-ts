import session from 'express-session';

const middleware = session({
  secret: process.env.SESSION_SECRET || 'secret',
  resave: true,
  saveUninitialized: false,
  cookie: {
    secure: process.env.NODE_ENV === 'production',
    maxAge: 1500000000,
    httpOnly: true,
  },
});

export default middleware;
