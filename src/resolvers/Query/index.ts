import { queryType } from 'nexus';

import users from './users';

const Query = queryType({
  definition(t) {
    users(t);
  },
});

export { Query };
