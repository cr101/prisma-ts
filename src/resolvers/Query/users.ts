import { getUserId } from '../../utils/auth';
import { QueryDefinition, QueryResolver } from '../../types/Resolver';

const me: QueryResolver<'me'> = async (parent, args, ctx) => {
  const userId = await getUserId(ctx);
  return ctx.photon.users.findOne({
    where: {
      id: userId,
    },
  });
};

const users: QueryDefinition = (t) => {
  t.field('me', {
    type: 'User',
    nullable: true,
    resolve: me,
  });
};

export default users;
