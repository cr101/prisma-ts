import { stringArg } from 'nexus';
import { hash } from 'bcryptjs';
import { ForbiddenError } from 'apollo-server-express';

import { validateUser } from '../../utils/auth';
import { MutationDefinition, MutationResolver } from '../../types/Resolver';

const register: MutationResolver<'register'> = async (parent, { email, password }, ctx) => {
  if (ctx.request.session?.user?.id) {
    throw new ForbiddenError('Already logged in');
  }

  const hashedPassword = await hash(password, 10);

  let user;
  try {
    user = await ctx.photon.users.create({
      data: {
        email,
        password: hashedPassword,
      },
    });
  } catch (e) {
    throw new ForbiddenError('User already exists');
  }

  if (ctx.request.session) {
    ctx.request.session.user = user;
  }

  return user;
};

const login: MutationResolver<'login'> = async (parent, { email, password }, ctx) => {
  if (ctx.request.session?.user?.id) {
    throw new ForbiddenError('Already logged in');
  }
  const user = await validateUser(email, password, ctx);
  if (ctx.request.session) {
    // eslint-disable-next-line no-param-reassign
    ctx.request.session.user = user;
  }
  return user;
};

const logout: MutationResolver<'logout'> = async (parent, args, ctx) => {
  const user = ctx.request.session?.user;
  if (ctx.request.session) {
    ctx.request.session.destroy((err) => {
      if (err) {
        throw new Error(err);
      }
    });
  }
  return user;
};

const auth: MutationDefinition = (t) => {
  t.field('register', {
    type: 'User',
    args: {
      email: stringArg({ nullable: false }),
      password: stringArg({ nullable: false }),
    },
    resolve: register,
  });

  t.field('login', {
    type: 'User',
    args: {
      email: stringArg({ nullable: false }),
      password: stringArg({ nullable: false }),
    },
    resolve: login,
  });
  t.field('logout', {
    type: 'User',
    resolve: logout,
  });
};

export default auth;
