import { Photon, User } from '@prisma/photon';

const photon = new Photon();

interface Package {
  user: User;
  name: string;
  desc: string;
  versions: string[];
}

const generatePackage = async (data: Package): Promise<void> => {
  const pkg = await photon.packages.create({
    data: {
      author: {
        connect: { id: data.user.id },
      },
      name: data.name,
      description: data.desc,
      versions: {
        set: data.versions,
      },
    },
  });
  // eslint-disable-next-line no-console
  console.log({ pkg });
};

async function main(): Promise<void> {
  const user1 = await photon.users.create({
    data: {
      email: 'alice@prisma.io',
      password: '$2b$10$ZjONRZAxqX2pLoPax2xdcuzABTUEsFanQI6yBYCRtzpRiU4/X1uIu', // "graphql"
    },
  });
  const user2 = await photon.users.create({
    data: {
      email: 'bob@prisma.io',
      password: '$2b$10$o6KioO.taArzboM44Ig85O3ZFZYZpR3XD7mI8T29eP4znU/.xyJbW', // "secret43"
    },
  });

  const pkgsData = [
    {
      name: 'Boost', user: user1, desc: 'C++ utils', versions: ['1.0.2'],
    },
    {
      name: 'Network lib', user: user1, desc: 'network library, with a message system', versions: ['2.0.0'],
    },
    {
      name: 'FTP Server', user: user1, desc: 'Basic server FTP', versions: ['0.4.7'],
    },
    {
      name: 'ECS', user: user2, desc: 'Entity - Component - System', versions: ['1.8.4'],
    },
    {
      name: 'Custom printer', user: user2, desc: 'Custom printer for pretty logs, with colors', versions: ['1.5.8'],
    },
  ];

  await Promise.all(pkgsData.map((data) => generatePackage(data)));
  // eslint-disable-next-line no-console
  console.log({ user1, user2 });
}

main().finally(async () => {
  await photon.disconnect();
});
